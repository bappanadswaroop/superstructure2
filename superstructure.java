package frc.robot.subsystems;

import java.util.List;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class SuperStructure extends SubsystemBase {
    private TalonFX intake, armL, armR, telescoping;
    private CANSparkMax wrist;
    private RelativeEncoder encoder;

    private PIDController wPID,armPID;
    private final SlewRateLimiter limiter;
    private final double armLength, armRot;


    public SuperStructure() {
        intake = TalonFX(/*Constants.motors.intake*/);
        telescoping = TalonFX(/*Constants.motors.telescoping_arm*/);
        armL = TalonFX(/*Constants.motors.armLeft*/);
        armR = TalonFX(/*Constants.motors.armRight*/);

        encoder = wrist.getEncoder();
        //encoder.multiply_nativeUnits_by_ConversionFactor to get a readable signal

        armR.follow(armL);
        armL.setInverted(InvertType.OpposeMaster);

        //configure by tweaking
        wristPID = new PIDController(/*p,i,d*/);
        armPID = new PIDController(/*kp, ki, kd*/);

    }

    public void periodic() {
        //figure this part out later, operate the cansparkmax and mess around with it and work around the driverstation stuff
    }

    public void setArm(double length) {
        telescoping.set(ControlMode.Position, /*conversion dimensional analysis*/);
        armLength = length; 
    }

    public void setRotation(double degrees) {
        armL.set(ControlMode.Position, /*degrees to motor ticks conversion*/);
        armRot = degrees;
    }

    public void intake() {
        //ask criz why the outputs are opposite for cone/cube (+/-)
    }

    public void stopIntake() {
        intake.neutralOutput();
    }

    public double getArmLength() {
        return armLength;
    }

    public double getArmState() {
        return armRot;
    }

    public void reset() {
        encoder.setPosition(/*intial wrist position*/);
        wPID.reset(); 
        wPID.setSetpoint(/*initial wrist position*/);
        armL.set(ControlMode.Position, /*intial arm position*/);
        telescoping.set(ControlMode.Position, 0); //change this??
    }

    public void stop() {
        armL.neutralOutput();
        telescoping.neutralOutput();
        intake.neutralOutput();
        wrist.set(0);
        wPID.reset();
    }

    
}
